import pygame
import pynput
import sys

from character_set import CharacterSet

class Game:

    def __init__(self):
        self.game = pygame.init()
        self.screen = pygame.display.set_mode((528, 400))
        self.character_set = CharacterSet(self)
        pygame.display.set_caption("*internal screaming*")

        # character speed
        self.change_pos = 3
        self.total_change = 0
        self.moving = False

        # character pos
        self.char_x = 66
        self.char_y = 330

        # level
        self.level_num = 1

        # clock
        self.clock = pygame.time.Clock()
        self.clock.tick(60)

        # sprite iteration
        self.iteration_num = 0

    def run_game(self):
        with pynput.keyboard.Listener(
                on_press=self.on_press,
                on_release=self.on_release) as listener:

            while True:
                self._check_events()
                self._update_screen()


    def on_press(self, key):
        try:
            print('alphanumeric key {0} pressed'.format(key.char))
            pass

        except AttributeError:
            # print('special key {0} pressed'.format(key))
            if str(key) == "Key.right":
                if (self.char_x + 3) >= 499:
                    self.change_pos = 0
                else:
                    self.change_pos = 3
                self._move_char_x(self.change_pos)

            elif str(key) == "Key.left":
                if (self.char_x - 3) <= 0:
                    self.change_pos = 0
                else:
                    self.change_pos = 3
                self._move_char_x(-1 * self.change_pos)

            elif str(key) == "Key.up":
                if (self.char_y + 3) <= 0:
                    self.change_pos = 0
                else:
                    self.change_pos = 3

                self._move_char_y(-1 * self.change_pos)

            elif str(key) == "Key.down":
                if (self.char_y - 3) >= 327:
                    self.change_pos = 0
                else:
                    self.change_pos = 3
                self._move_char_y(self.change_pos)

    def on_release(self, key):
        # print('{0} released'.format(key))
        while self.char_y != 330:
            self._move_char_y(self.change_pos)
            pygame.time.delay(30)

        if key == pynput.keyboard.Key.esc:
            # Stop listener
            pygame.quit()
            sys.exit()

    def _check_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

    def _move_char_x(self, dir):
        self.moving = True

        self.char_x += dir

        # print(self.char_x)

    def _move_char_y(self, dir):
        self.moving = True

        self.char_y += dir

        # print(self.char_y)

    def _update_screen(self):
        self.screen.fill((0, 154, 255))

        if self.level_num == 1:
            # Character
            if self.iteration_num <= 10:
                self.character_set.pieces[self.iteration_num].blitme(self.char_x, self.char_y)
                self.iteration_num += 1
            else:
                self.iteration_num = 0

            # Terrain
            self.xterrain = 0
            while self.xterrain < 528:
                self.character_set.pieces[11].blitme(self.xterrain, 354)
                self.xterrain += 48

            # Platforms
            self.character_set.pieces[12].blitme(200, 300)
            self.character_set.pieces[13].blitme(300, 300)

        elif self.level_num == 2:
            pass
        elif self.level_num == 3:
            pass
        elif self.level_num == 0:
            pass

        pygame.display.flip()

if __name__ == '__main__':
    game = Game()
    game.run_game()