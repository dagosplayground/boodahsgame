from spritesheet import SpriteSheet

class CharacterSet:

    def __init__(self, game):

        self.game = game
        self.pieces = []
        self._load_pieces()

    def _get_char_x(self, iteration_num):
        self.char_x = 32 * iteration_num + 5
        return self.char_x

    def _create_char_iteration(self, iteration_num):
        filename_char = 'images/Free/Main Characters/Virtual Guy/Idle (32x32).png'
        character_ss = SpriteSheet(filename_char)

        self.char_rect_y = 8
        self.char_height = 24
        if iteration_num == 3 or iteration_num == 4:
            self.char_rect_y = 7
            self.char_height = 25
        elif 5 <= iteration_num <= 8:
            self.char_rect_y = 6
            self.char_height = 26
        elif iteration_num == 9 or iteration_num == 10:
            self.char_rect_y = 7
            self.char_height = 25

        character_rect = (self._get_char_x(iteration_num), self.char_rect_y, 22, self.char_height)
        character_image = character_ss.image_at(character_rect)

        character = Piece(self.game)
        character.image = character_image
        character.name = 'Player'
        self.pieces.append(character)

    def _load_pieces(self):

        # Character
        self._create_char_iteration(0)
        self._create_char_iteration(1)
        self._create_char_iteration(2)
        self._create_char_iteration(3)
        self._create_char_iteration(4)
        self._create_char_iteration(5)
        self._create_char_iteration(6)
        self._create_char_iteration(7)
        self._create_char_iteration(8)
        self._create_char_iteration(9)
        self._create_char_iteration(10)

        # Terrain
        filename_terrain = 'images/Free/Terrain/Terrain (16x16).png'
        terrain_ss = SpriteSheet(filename_terrain)

        terrain_rect = (96, 0, 48, 48)
        terrain_image = terrain_ss.image_at(terrain_rect)

        terrain = Piece(self.game)
        terrain.image = terrain_image
        terrain.name = 'Terrain'
        self.pieces.append(terrain)

        # Platforms
        filename_platform = 'images/Free/Terrain/Terrain (16x16).png'
        platform_ss = SpriteSheet(filename_platform)

        platform_rect0 = (272, 16, 48, 5)
        platform_image0 = platform_ss.image_at(platform_rect0)

        platform0 = Piece(self.game)
        platform0.image = platform_image0
        platform0.name = 'Platform 0'
        self.pieces.append(platform0)

        platform_image1 = platform_ss.image_at(platform_rect0)

        platform1 = Piece(self.game)
        platform1.image = platform_image1
        platform1.name = 'Platform 1'
        self.pieces.append(platform1)



class Piece:

    def __init__(self, game):
        self.image = None
        self.name = ''

        self.screen = game.screen

    def blitme(self, x, y):

        self.rect = self.image.get_rect()
        self.rect.topleft = x, y
        self.screen.blit(self.image, self.rect)

